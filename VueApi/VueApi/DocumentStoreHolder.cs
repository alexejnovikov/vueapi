﻿using Microsoft.Extensions.Options;
using Raven.Client.Documents;
using Raven.Client.ServerWide;
using Raven.Client.ServerWide.Operations;
using VueApi.Interfaces;

namespace VueApi
{
    public class DocumentStoreHolder : IDocumentStoreHolder
    {
        public DocumentStoreHolder(IOptions<RavenSettings> ravenSettings)
        {
            var settings = ravenSettings.Value;
            Store = new DocumentStore
            {
                Urls = new[] { settings.Url },
                Database = settings.Database,
            };
            Store.Initialize();

            CreateDataBaseIfNotExists();
        }

        public IDocumentStore Store { get; }

        private void CreateDataBaseIfNotExists()
        {
            var database = Store.Database;
            var dbRecord = Store.Maintenance.Server.Send(new GetDatabaseRecordOperation(database));

            if (dbRecord == null)
            {
                var db = new DatabaseRecord(database);
                Store.Maintenance.Server.Send(new CreateDatabaseOperation(db));
            }
        }
    }
}
