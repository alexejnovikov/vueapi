﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VueApi.Interfaces;
using VueApi.ViewModels;

namespace VueApi.Controllers
{
    [Route("api/list-items")]
    [Authorize]
    public class ListItemsController : ControllerBase
    {
        private readonly IItemService itemService;

        public ListItemsController(IItemService itemService)
        {
            this.itemService = itemService;
        }

        /// <summary>
        /// Gets all ToDoItems for logged in user.
        /// </summary>
        /// <response code="200">Returns ToDoitem collection.</response>
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            return Ok(await itemService.GetItems(User.Identity.Name));
        }

        /// <summary>
        /// Creates a ToDoItem.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /list-items
        ///     {
        ///        "id": 1,
        ///        "title": "Item1",
        ///        "isDone": true
        ///     }.
        ///
        /// </remarks>
        /// <param name="model">New ToDoItem data.</param>
        /// <returns>A newly created ToDoItem Id.</returns>
        /// <response code="200">Returns the newly created item Id.</response>
        /// <response code="400">If the item is null.</response>
        [HttpPost]
        public async Task<ActionResult> Post([FromBody]ItemViewModel model)
        {
            if (ModelState.IsValid)
            {
                return Ok(await itemService.CreateToDoItem(User.Identity.Name, model));
            }

            return BadRequest(ModelState);
        }

        /// <summary>
        /// Updates a ToDoItem.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /list-items
        ///     {
        ///        "id": 1,
        ///        "title": "Item1",
        ///        "isDone": true
        ///     }.
        ///
        /// </remarks>
        /// <param name="id">Updated ToDoItem Id.</param>
        /// <param name="model">New ToDoItem data.</param>
        /// <response code="200">Update was successfully completed.</response>
        /// <response code="400">If the validation is not correct.</response>
        /// <response code="404">If the item is not found.</response>
        [HttpPut]
        public async Task<ActionResult> Put(string id, [FromBody]ItemViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (await itemService.UpdateItem(id, model))
                {
                    return Ok();
                }

                return NotFound();
            }

            return BadRequest(ModelState);
        }

        /// <summary>
        /// Delete a ToDoItem.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /list-items
        ///     {
        ///        "id": 1,
        ///     }.
        ///
        /// </remarks>
        /// <param name="id">Updated ToDoItem Id.</param>
        /// <response code="200">Delete was successfully completed.</response>
        /// <response code="404">If the item is not found.</response>
        [HttpDelete]
        public async Task<ActionResult> Delete(string id)
        {
            if (await itemService.RemoveItem(id))
            {
                return Ok();
            }

            return NotFound();
        }

        /// <summary>
        /// Remove all completed ToDoItems for logged in User.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /list-items/clear.
        ///
        /// </remarks>
        /// <response code="200">Delete was successfully completed.</response>
        [HttpDelete]
        [Route("clear")]
        public async Task<ActionResult> ClearItems()
        {
            await itemService.RemoveDoneItems(User.Identity.Name);
            return Ok();
        }
    }
}
