﻿namespace VueApi.Models
{
    public class ToDoItem
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public bool IsDone { get; set; }

        public string Email { get; set; }
    }
}
