﻿namespace VueApi
{
    public class RavenSettings
    {
        public string Url { get; set; }

        public string Database { get; set; }
    }
}
