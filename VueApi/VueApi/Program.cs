﻿using System;
using System.IO;
using System.Net;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace VueApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddJsonFile(CreateGoogleKeysJsonFile());
                })
                .UseStartup<Startup>();

        private static string CreateGoogleKeysJsonFile()
        {
            var client = new WebClient();
            var data = client.DownloadString("https://www.googleapis.com/oauth2/v3/certs");
            var path = Environment.CurrentDirectory + @"\keyssettings.json";
            using (var sw = new StreamWriter(path))
            {
                sw.WriteLine(data);
            }

            return path;
        }
    }
}
