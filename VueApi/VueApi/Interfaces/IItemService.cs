﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VueApi.Models;
using VueApi.ViewModels;

namespace VueApi.Interfaces
{
    public interface IItemService
    {
        Task<string> CreateToDoItem(string userName, ItemViewModel model);

        Task<List<ToDoItem>> GetItems(string userName);

        Task<bool> UpdateItem(string id, ItemViewModel model);

        Task<bool> RemoveItem(string id);

        Task RemoveDoneItems(string userName);
    }
}