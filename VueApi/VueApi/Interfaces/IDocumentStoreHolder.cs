﻿using Raven.Client.Documents;

namespace VueApi.Interfaces
{
    public interface IDocumentStoreHolder
    {
        IDocumentStore Store { get; }
    }
}