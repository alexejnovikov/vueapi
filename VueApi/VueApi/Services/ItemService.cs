﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Raven.Client.Documents;
using Raven.Client.Documents.Operations;
using Raven.Client.Documents.Queries;
using VueApi.Interfaces;
using VueApi.Models;
using VueApi.ViewModels;

namespace VueApi.Services
{
    public class ItemService : IItemService
    {
        private readonly IDocumentStore store;

        public ItemService(IDocumentStoreHolder documentStoreHolder)
        {
            store = documentStoreHolder.Store;
        }

        public async Task<string> CreateToDoItem(string userName, ItemViewModel model)
        {
            using (var session = store.OpenAsyncSession())
            {
                var item = new ToDoItem
                {
                    Title = model.Title,
                    IsDone = model.IsDone,
                    Email = userName,
                };
                await session.StoreAsync(item);
                await session.SaveChangesAsync();
                return item.Id;
            }
        }

        public async Task<List<ToDoItem>> GetItems(string userName)
        {
            using (var session = store.OpenAsyncSession())
            {
                return await session.Query<ToDoItem>().Where(w => w.Email == userName).OrderBy(o => o.Id).ToListAsync();
            }
        }

        public async Task<bool> UpdateItem(string id, ItemViewModel model)
        {
            using (var session = store.OpenAsyncSession())
            {
                var data = await session.LoadAsync<ToDoItem>(id);
                if (data != null)
                {
                    data.IsDone = model.IsDone;
                    data.Title = model.Title;
                    await session.SaveChangesAsync();
                }

                return data != null;
            }
        }

        public async Task<bool> RemoveItem(string id)
        {
            using (var session = store.OpenAsyncSession())
            {
                var data = await session.LoadAsync<ToDoItem>(id);
                if (data != null)
                {
                    session.Delete(data);
                    await session.SaveChangesAsync();
                }

                return data != null;
            }
        }

        public async Task RemoveDoneItems(string userName)
        {
            var operation = await store.Operations.SendAsync(new DeleteByQueryOperation(
                new IndexQuery
                {
                    Query = "from ToDoItems where Email = '" + userName + "' and IsDone = true",
                }));
            await operation.WaitForCompletionAsync();
        }
    }
}
