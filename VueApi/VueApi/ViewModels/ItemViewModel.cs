﻿using System.ComponentModel.DataAnnotations;

namespace VueApi.ViewModels
{
    public class ItemViewModel
    {
        [MaxLength(25)]
        public string Title { get; set; }

        public bool IsDone { get; set; }
    }
}
